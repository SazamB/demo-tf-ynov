variable "server_type" {
  description = "Instance server type"
  type = object(
    {
      app_srv_type = string
      es_srv_type = string
    }
  )
  default = {
    app_srv_type = "DEV1-XL"
    es_srv_type = "DEV1-S"
  }
}

variable "firstname" {
  description = "User firstname"
  type = string
  default = "Samy"
}

variable "private_key_path" {
  description = "Private SSH key file path"
  type = string
  default = "/home/sazam/.ssh/scaleway2"
}

variable "public_key_path" {
  description = "Public Key"
  type = string
  default = "/home/sazam/.ssh/scaleway2.pub"
}