locals {
  servers={
    es={
        name="${var.firstname}"
    }
    es1={
        name="${var.firstname}-1"
    },
    es2={
        name="${var.firstname}-2"
    },
    app={
        name="${var.firstname}-3"
    }
  }
}
# variable "servers" {                                                                                                                                 
#   default = {                                                                                                                                        
#     app = 1                                                                                                                                          
#     es = 3                                                                                                                                           
#   }                                                                                                                                                  
# }                                                                                                                                                    
                                                                                                                                                     
# locals {                                                                                                                                             
#   firstname = "Samy"                                                                                                                            
#   # Generate list of servers name                                                                                                                    
#   servers = concat(                                           # Merge all hostnames lists                                                            
#     [                                                                                                                                                
#       for k,v in var.servers: [                               # Iterate on each server type                                                          
#         for idx in range(v): "${local.firstname}-${k}-${idx}" # Create a list of name based of expected number of servers                            
#       ]                                                                                                                                              
#     ]...                                                      # Use items reference (spread operator)                                                
#   )                                                                                                                                                  
# }
