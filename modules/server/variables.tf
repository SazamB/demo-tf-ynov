variable "server_name" {
  description = "Server name"
  type = string
  default = "samy"
}
variable "server_type" {
  description = "Server type"
  type = string
}
variable "secgroup_id" {
  description = "Security group ID"
  type = string
}
variable "vpc_id" {
  description = "Private network ID"
  type = string
}
variable "private_key_path_module" {
  description = "Private SSH key file path"
  type = string
  
}

