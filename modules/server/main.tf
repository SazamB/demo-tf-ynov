terraform {
  required_providers {
    null = {
      source = "hashicorp/null"
      version = "3.2.1"
    }
    scaleway = {
      source = "scaleway/scaleway"
      version = "2.9.1"
    }
  }
  required_version = "~> 1.3.0"
}

locals {
  install_docker = [
    "apt-get remove docker docker-engine docker.io containerd runc",
    "apt-get update",
    "mkdir -p /etc/apt/keyrings",
    "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor > /etc/apt/keyrings/docker.gpg",
    "echo 'deb [arch=amd64 signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu jammy stable' | tee /etc/apt/sources.list.d/docker.list",
    "apt-get update",
    "apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin",
  ]
}

resource "scaleway_instance_ip" "ip" {}


resource "scaleway_instance_volume" "disk" {
  size_in_gb = 30
  type = "b_ssd"
}

resource "scaleway_instance_server" "srv" {
  name = var.server_name
  type = var.server_type
  image = "ubuntu_jammy"
  ip_id = scaleway_instance_ip.ip.id
  additional_volume_ids = [ scaleway_instance_volume.disk.id ]
  security_group_id = var.secgroup_id
  

  private_network {
    pn_id = var.vpc_id
  }
}

resource "null_resource" "script" {
  triggers = {
    instance_id = scaleway_instance_server.srv.id
  }

  connection {
    host = scaleway_instance_ip.ip.address
    private_key = file(var.private_key_path_module)
  }

  provisioner "remote-exec" {
    inline = local.install_docker
  }
}
