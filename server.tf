resource "scaleway_account_ssh_key" "user_ssh_pub" {
    name        = var.firstname
    public_key = file(var.public_key_path)

}

resource "scaleway_instance_security_group" "es_secgroup" {
  inbound_default_policy = "drop"

  inbound_rule {
    action = "accept"
    port   = 22
  }
}

resource "scaleway_instance_security_group" "app_secgroup" {
  inbound_default_policy = "drop"

  inbound_rule {
    action = "accept"
    port   = 22
  }

  inbound_rule {
    action = "accept"
    port   = 3000
  }

  inbound_rule {
    action = "accept"
    port   = 5601
  }

  inbound_rule {
    action = "accept"
    port   = 9090
  }
}

module "servers" {
  for_each = local.servers
  source = "./modules/server"
  server_name = "${each.value.name}"
  server_type =each.key=="app" ? var.server_type.app_srv_type :var.server_type.es_srv_type
  secgroup_id =each.key=="app" ? scaleway_instance_security_group.app_secgroup.id : scaleway_instance_security_group.es_secgroup.id
  vpc_id = scaleway_vpc_private_network.vpc.id
  private_key_path_module = var.private_key_path
}

