terraform {
  required_providers {
    scaleway = {
      source = "scaleway/scaleway"
      version = "2.9.1"
    }
  }
  required_version = "~> 1.3.0"

  backend "s3" {
      bucket                      = "backends-terraform-98c5d44c32f9"
      key                         = "samy.tfstate"
      region                      = "fr-par"
      endpoint                    = "https://s3.fr-par.scw.cloud"
      skip_credentials_validation = true
      skip_region_validation      = true
    }

    
}




provider "scaleway" {
  # Configuration options
}
